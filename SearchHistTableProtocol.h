//
//  SearchHistTableProtocol.h
//  AllDigitalTest
//
//  Created by JJ on 3/30/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SearchHistTableProtocol <NSObject>

/*
 * Call this method to hide the clear history button when there are no more recent history
 */

-(void)removeFinalRowCallback;

/*
 * Call this method when a row is selected
 */
-(void)recentHistRowSelected :(NSString *)rowValue;

@end
