//
//  SearchController.m
//  AllDigitTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import "SearchController.h"
#import "Reachability.h"

@implementation SearchController

BOOL isShowingNoInternetScreen;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
 * Search bar delegates
 */

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    //Show the filter button
   // self.searchBar.showsCancelButton = YES;
    //[self changeSearchBarCancelBtnTitle];
    
    //Display the search subview background
    self.searchBackgroundSubView.hidden = false;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    /*
     * Checking for internet status
     * Alert the user if there is no internet connectivity
     */
    
    Reachability *internetReach = [Reachability reachabilityWithHostname:@"www.google.com"];
    NetworkStatus internetStatus = [internetReach currentReachabilityStatus];
    
    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN))
    {
        UIAlertView *noConnection = [[UIAlertView alloc]initWithTitle:@"No internet connection" message:@"Please connect to the internet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        
        [noConnection show];
        
    }
    
    else
    {
        /*
         * Replace the no search history text
         * If there are values inside the array, just insert the values in the beginning
         */
        
        if(self.histTableViewController.recentHistoryArry.count > 0 && [[self.histTableViewController.recentHistoryArry objectAtIndex:0] isEqualToString:noRecentHistory])
        {
            [self.histTableViewController.recentHistoryArry replaceObjectAtIndex:0 withObject:searchBar.text];
        }
        
        else
        {
            [self.histTableViewController.recentHistoryArry insertObject:searchBar.text atIndex:0];
 
        }
        
        //We have have more than 50 recent search history, remove the oldest search history
        if(self.histTableViewController.recentHistoryArry.count > 50)
        {
            [self.histTableViewController.recentHistoryArry removeLastObject];
        }
        
        //Save the new history array
        [self.loadHistory setObject:self.histTableViewController.recentHistoryArry forKey:@"userhistory"];
        
        [searchBar resignFirstResponder];
        [self.delegate searchClickDelegate:searchBar.text];
    }

}

/*
 * Hides the keyboard, filter button, and the search sub background view
 */

-(void)handleTap:(UISwipeGestureRecognizer *)recgonizer
{

  //  self.searchBar.showsCancelButton = NO;
    self.searchBackgroundSubView.hidden = true;
    
    [self.searchBar resignFirstResponder];
}

/*
 * Call this method when a row is selected
 */
-(void)recentHistRowSelected :(NSString *)rowValue
{
    /*
     * Checking for internet status
     * Alert the user if there is no internet connectivity
     */
    
    Reachability *internetReach = [Reachability reachabilityWithHostname:@"www.google.com"];
    NetworkStatus internetStatus = [internetReach currentReachabilityStatus];
    
    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN))
    {
        UIAlertView *noConnection = [[UIAlertView alloc]initWithTitle:@"No internet connection" message:@"Please connect to the internet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        
        [noConnection show];
        
    }
    
    else
    {
        /*
         * Replace the no search history text
         * If there are values inside the array, just insert the values in the beginning
         */
        
          if(self.histTableViewController.recentHistoryArry.count > 0 && [[self.histTableViewController.recentHistoryArry objectAtIndex:0] isEqualToString:noRecentHistory])
        {
            [self.histTableViewController.recentHistoryArry replaceObjectAtIndex:0 withObject:rowValue];
        }
        
        else
        {
            [self.histTableViewController.recentHistoryArry insertObject:rowValue atIndex:0];
            
        }
        
        //We have have more than 50 recent search history, remove the oldest search history
        if(self.histTableViewController.recentHistoryArry.count > 50)
        {
            [self.histTableViewController.recentHistoryArry removeLastObject];
        }
        
        //Save the new history array
        [self.loadHistory setObject:self.histTableViewController.recentHistoryArry forKey:@"userhistory"];

        [self.delegate searchClickDelegate:rowValue];
    }

}

/*
 * Clear search history event
 */

-(void)removeHistoryEvent
{
    [self.histTableViewController.recentHistoryArry removeAllObjects];
    
    [self.histTableViewController.tableView reloadData];
    
    self.clearHistoryBtn.hidden = true;
    
     self.histTableViewController.tableView.frame = CGRectMake(0, self.searchBar.frame.origin.y + self.searchBar.frame.size.height, self.viewManager.screenSize.width, self.viewManager.screenSize.height - self.searchBar.frame.size.height );
    self.histTableViewController.tableView.userInteractionEnabled = false;
}

/*
 * Call this method to hide the clear history button when there are no more recent history
 */
-(void)removeFinalRowCallback
{
    
    self.clearHistoryBtn.hidden = true;
    
    self.histTableViewController.tableView.frame = CGRectMake(0, self.searchBar.frame.origin.y + self.searchBar.frame.size.height, self.viewManager.screenSize.width, self.viewManager.screenSize.height - self.searchBar.frame.size.height );

    self.histTableViewController.tableView.userInteractionEnabled = false;
}

@end
