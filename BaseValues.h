//
//  BaseValues.h
//  AllDigitalTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//
//  Purpose: The base class initialize all the controllers' methods and values

#import <Foundation/Foundation.h>
#import "BaseValueProtocol.h"
#import "ViewManager.h"
#import "Constant.h"
#import "SearchController.h"
#import "SearchResultController.h"
#import "ConManager.h"

@interface BaseValues : NSObject<BaseValueProtocol>

/*
 * Class objects
 */

@property(nonatomic, retain) ViewManager *viewManager;
@property(nonatomic, retain) ConManager *conManager;

@property(nonatomic, retain) SearchController *searchController;
@property(nonatomic, retain) SearchResultController *searchResultController;


/*
 * Control values for animation
 */

@property(nonatomic) BOOL isAnimating;

@end
