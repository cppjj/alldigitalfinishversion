//
//  ConManagerProtocol.h
//  AllDigitalTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//
//  Purpose: Declares all the connection manager methods

#import <Foundation/Foundation.h>

@protocol ConManagerProtocol <NSObject>

//Handles asychronous loading
-(void)sendAsyRequest:(NSString *)urlString :(int)returnMenu;

//Config call back method
-(void)getSearchResultCallBack:(NSDictionary *)returnDictionaryValue;

@end
