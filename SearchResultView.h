//
//  SearchResultView.h
//  AllDigitalTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//
// Purpose: Initialize all the search result UIs here

#import <UIKit/UIKit.h>
#import "CustomUI.h"
#import "SearchResultProtocol.h"
#import "Constant.h"
#import "ViewManager.h"
#import "ConManager.h"
#import "ResultTableViewController.h"
#import "JsonDecode.h"
#import "ResultDetailController.h"

@interface SearchResultView : UIView<SearchResultProtocol>

@property(nonatomic) id delegate;

/*
 * Class objects
 */

@property(nonatomic, retain) ViewManager *viewManager;
@property(nonatomic, retain) ConManager *conManager;
@property(nonatomic, retain) CustomUI *customUI;
@property(nonatomic, retain) JsonDecode *jsonDecode;

@property(nonatomic, retain) ResultDetailController *resultDetailController;
@property(nonatomic, retain) ResultTableViewController *resultTableController;

/*
 * Search Result detail properties
 */

@property(nonatomic) BOOL isAnimating;
@property(nonatomic) BOOL isMovingUp;

/*
 * UIView
 */

@property(nonatomic, retain) UIView *searchResultTitleBackground;

/*
 * UIButton
 */

@property(nonatomic, retain) UIButton *searchResultHomeBtn;
@property(nonatomic, retain) UIButton *searchResultBackBtn;

/*
 * Loading indicator view
 */

@property(nonatomic, retain) UIActivityIndicatorView *resultLoadingIndicator;


/*
 * UILabels
 */

@property(nonatomic, retain) UILabel *resultBackArrowLabel;
@property(nonatomic, retain) UILabel *resultBackTextLabel;
@property(nonatomic, retain) UILabel *resultTitleLabel;

@end
