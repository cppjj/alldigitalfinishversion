//
//  Constant.h
//  AllDigitTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//
//  Purpose: Holds all the constants values for all classes

#import <Foundation/Foundation.h>

/*
 * Declares all the menu constants
 * Using it to determine which menu the user is current in
 */

static const int SearchMenuViewConst = 1000;
static const int SearchResMenuViewConst = 1001;
static const int SearchDetMenuViewConst = 1002;

/*
 * Declares the endpoint url
 */

static NSString *endPointURL = @"https://itunes.apple.com/search?term=";
static NSString *endPointParameters = @"&media=movie&entity=movie";

/*
 * Search result strings
 */

static NSString *searchResultBackArrow = @"〈";
static NSString *searchResultBackString = @"Back";
static NSString *searchResultTitle = @"Movie Result";

//Search result tableview string
static NSString *noResultString = @"No result found!";

//Recent history tablview string
static NSString *noRecentHistory = @"No search history";

@interface Constant : NSObject

@end
