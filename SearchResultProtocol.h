//
//  SearchResultProtocol.h
//  AllDigitalTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewManager.h"
#import "Constant.h"
#import "ConManager.h"
#import "SearchResult.h"

@protocol SearchResultProtocol <NSObject>

/*
 * Initialize all the search result view UIs
 */

-(void)searchResultViewInit:(ViewManager *)myViewManager :(ConManager *)myConManger;

/*
 * Call this method to load the movies from the server
 */

-(void)loadServerMovies:(NSString *)searchTerm;

/*
 * Use this method to initialize the movie detail view
 */

-(void)detailViewInitMethod :(SearchResult *)holdSearchResult;

/*
 * Use this method to remove the movie detail view
 */

-(void)removeDetailViewMethod;

/*
 * Delegate call back methods when user clicks on the back button
 */

-(void)backBtnPressedDelegate;
-(void)homeBtnPressedDelegate;

@end
