//
//  ResultTableProtocol.h
//  AllDigitalTest
//
//  Created by JJ on 3/29/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SearchResult.h"

@protocol ResultTableProtocol <NSObject>

//Call back method when row is selected
-(void)getSelectedResultRow : (SearchResult *)holdSearchResult;

//Scroll down call back method for result title animation
-(void)scrollDownCallBack;

//Scroll up call back method for result title animation
-(void)scrollUpCallBack;

@end
