//
//  ConManager.h
//  AllDigitalTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//
//  Purpose: Handles all the connection to the server

#import <Foundation/Foundation.h>
#import "Constant.h"
#import "ConManagerProtocol.h"

@interface ConManager : NSObject<ConManagerProtocol>

@property (nonatomic) id delegate;

@property (nonatomic) NSMutableData *responseData;
@property (nonatomic) NSString *stringRespond;

@end
