//
//  ViewManagerProtocol.h
//  AllDigitalTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//
//  Purpose: Creates all the required view manager methods

#import <Foundation/Foundation.h>

@protocol ViewManagerProtocol <NSObject>

/*
 * The method convert the menu into a string value for storing purpose
 */
-(NSString *)convertMenu:(int)menuSelected;

@end
