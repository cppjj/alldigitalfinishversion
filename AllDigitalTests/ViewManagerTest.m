//
//  ViewManagerTest.m
//  AllDigitalTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ViewManager.h"

@interface ViewManagerTest : XCTestCase

@property(nonatomic) ViewManager *viewManager;

@end

@implementation ViewManagerTest

- (void)setUp
{
    [super setUp];
    
    _viewManager = [[ViewManager alloc]init];
}

- (void)tearDown
{
    _viewManager = nil;
    
    [super tearDown];
}

-(void)testViewManagerExists
{
    XCTAssertNotNil(_viewManager, @"We are able to create view manager instance");
}

-(void)testConvertMenuEqual
{
    
    XCTAssertEqualObjects(@"1000", [_viewManager convertMenu:1000], @"Menu is equal");
}

-(void)testConvertMenuNotEqual
{
    
    XCTAssertNotEqualObjects(@"1001", [_viewManager convertMenu:1000], @"Menu is not equal");
    
}

@end
