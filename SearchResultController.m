//
//  SearchResultController.m
//  AllDigitalTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import "SearchResultController.h"

@implementation SearchResultController

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}

/*
 * Load the movies from the server
 */

-(void)loadServerMovies:(NSString *)searchTerm
{
    
    NSString *searchURL = endPointURL;
    
    NSString *searchParameters = searchTerm;
    
    //Replace all all the spaces with +
    searchParameters = [searchParameters stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    searchParameters = [searchParameters stringByAppendingString:endPointParameters];
    searchParameters = [searchParameters stringByAppendingString:@"&limit=200"];
    
    searchURL = [searchURL stringByAppendingString:searchParameters];
    
    [self.conManager sendAsyRequest:searchURL :SearchResMenuViewConst];
}

/*
 * Home button event method
 */

-(void)homeBtnPressed:(id)button
{
    [button setBackgroundImage:[UIImage imageNamed:@"HomeRelease"] forState:UIControlStateNormal];
    
}

-(void)homeBtnReleased:(id)button
{
     [button setBackgroundImage:[UIImage imageNamed:@"Home"] forState:UIControlStateNormal];
    
    [self.delegate homeBtnPressedDelegate];
}

/*
 * Back button event method
 */

-(void)searchResultBackEvent
{
    
    [self.delegate backBtnPressedDelegate];
}

/*
 * Call back method when finish loading loading json data
 */

-(void)getSearchResultCallBack:(NSDictionary *)returnDictionaryValue
{
    self.resultTableController.tableView.hidden = false;
    [self.resultLoadingIndicator stopAnimating];
    
    [self.jsonDecode decodeMovieInit:returnDictionaryValue];
    
    self.resultTableController.resultTableArray = [self.jsonDecode getDecodeResultArry];
    
    [self.resultTableController.tableView reloadData];
    
}

/*
 * Call back method when user clicked on a row from the result table view
 */

-(void)getSelectedResultRow : (SearchResult *)holdSearchResult
{
 
    [self detailViewInitMethod:holdSearchResult];
    
}

//Scroll down call back method for result title animation
-(void)scrollDownCallBack
{
    if(!self.isAnimating)
    {
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             
                             self.isAnimating = true;
                             
                             int currentDisplayView = [[self.viewManager.backListMenuAry lastObject]intValue];
                             
                             //Only animate here if the title bar view didn't move up yet
                             if(self.isMovingUp)
                             {
                                 if(currentDisplayView==SearchResMenuViewConst)
                                 {
                                     
                                     self.resultTableController.tableView.frame = CGRectMake(0, 0, self.viewManager.screenSize.width, self.viewManager.screenSize.height);
                                 }
                                 
                                 if(currentDisplayView==SearchDetMenuViewConst)
                                 {
                                     self.resultDetailController.frame = CGRectMake(0, 0, self.viewManager.screenSize.width, self.viewManager.screenSize.height);
                                     self.resultDetailController.detailScrollView.frame = CGRectMake(0, 0, self.viewManager.screenSize.width, self.viewManager.screenSize.height+50);
                                     self.resultDetailController.detailScrollView.contentSize = CGSizeMake(self.viewManager.screenSize.width,self.resultDetailController.detailLongDesLabel.frame.origin.y + self.resultDetailController.detailLongDesLabel.frame.size.height+100);
                                     
                                     [self setNeedsDisplay];

                                 }
                                 
                                self.searchResultTitleBackground.frame = CGRectMake(0, -50, self.viewManager.screenSize.width, 50);
                             }
                             
                         }
                         completion:^(BOOL finished){
                            
                             self.isAnimating = false;
                             self.isMovingUp = false;
                         }];

    }
    
}

//Scroll up call back method for result title animation
-(void)scrollUpCallBack
{
    
    if(!self.isAnimating)
    {
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             
                             self.isAnimating = true;
                             int currentDisplayView = [[self.viewManager.backListMenuAry lastObject]intValue];

                             //Only animate here if the title bar view didn't move down yet
                             if(self.isMovingUp==false)
                             {
                                 if(currentDisplayView==SearchResMenuViewConst)
                                 {
                                     self.resultTableController.tableView.frame = CGRectMake(0, 50, self.viewManager.screenSize.width, self.viewManager.screenSize.height - self.searchResultTitleBackground.frame.size.height);
                                 }
                                 
                                 if(currentDisplayView==SearchDetMenuViewConst)
                                 {
                                     self.resultDetailController.frame = CGRectMake(0, 50, self.viewManager.screenSize.width, self.viewManager.screenSize.height-self.searchResultTitleBackground.frame.size.height);
                                     self.resultDetailController.detailScrollView.frame = CGRectMake(0, 0, self.viewManager.screenSize.width, self.viewManager.screenSize.height-50);

                                 }
                                 
                                 self.searchResultTitleBackground.frame = CGRectMake(0, 0, self.viewManager.screenSize.width, 50);
                             }
                             
                         }
                         completion:^(BOOL finished){
                             
                             self.isAnimating = false;
                             self.isMovingUp = true;
                         }];
        
    }

}

@end
