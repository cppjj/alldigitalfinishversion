//
//  BaseController.m
//  AllDigitTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import "BaseController.h"
#import <MediaPlayer/MediaPlayer.h>

@implementation BaseController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
      
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /*
     * Initialize the main view values
     */
    
    _baseValues = [[BaseValues alloc]init];
    
    [_baseValues searchMenuInitialize];
    _baseValues.searchController.delegate = self;
    
    /*
     * Adding UIs to the main view
     */
    
    [self.view addSubview:_baseValues.searchController];
    


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

/*
 * Hides the status bar
 */

-(BOOL)prefersStatusBarHidden{
    return YES;
}

/*
 * Call this method when user finish back animation
 */
-(void)homeAnimationFinish
{
    [_baseValues.searchResultController removeFromSuperview];
    _baseValues.searchResultController = nil;
    
    [_baseValues.viewManager.backListMenuAry removeAllObjects];
    [_baseValues.viewManager.backListMenuAry addObject:[_baseValues.viewManager convertMenu:SearchMenuViewConst]];
    
    [_baseValues searchMenuInitialize];
    _baseValues.searchController.delegate = self;
    
    //Navigate user back to the search menu
    [_baseValues searchMenuInitialize];
    _baseValues.searchController.delegate = self;
    
    [self.view addSubview:_baseValues.searchController];
}

/*
 * Call this method to remove the current view the screen is displaying
 */

-(void)removeCurrentView
{

    if(_baseValues.viewManager.currentDisplayMenu == SearchMenuViewConst)
    {
        [_baseValues.searchController removeFromSuperview];
        _baseValues.searchController = nil;
        
    }
    
    if(_baseValues.viewManager.currentDisplayMenu  == SearchResMenuViewConst)
    {
        [_baseValues.searchResultController removeFromSuperview];
        _baseValues.searchResultController = nil;
        
        [_baseValues.viewManager.backListMenuAry removeAllObjects];
        [_baseValues.viewManager.backListMenuAry addObject:[_baseValues.viewManager convertMenu:SearchMenuViewConst]];
        
        [_baseValues searchMenuInitialize];
        _baseValues.searchController.delegate = self;

        [self.view addSubview:_baseValues.searchController];
    }
    
    if(_baseValues.viewManager.currentDisplayMenu == SearchDetMenuViewConst)
    {
        [_baseValues.searchResultController removeDetailViewMethod];
    }

}

/*
 * Handles all the delegate call back methods here
 */

//Delegate call back methods when user clicks on the search button
-(void)searchClickDelegate:(NSString *)searchTerm
{
    //Set the current display menu
    _baseValues.viewManager.currentDisplayMenu = [[_baseValues.viewManager.backListMenuAry lastObject]intValue];
    
    //Remove the current menu user is on
    [self removeCurrentView];
    
    //Append the route menu to the back list
    [_baseValues.viewManager.backListMenuAry addObject:[_baseValues.viewManager convertMenu:SearchResMenuViewConst]];
    
    [_baseValues searchResultMenuInitialize:searchTerm];

    _baseValues.searchResultController.delegate = self;
    
    [self.view addSubview:_baseValues.searchResultController];

}

-(void)backBtnPressedDelegate
{

    _baseValues.viewManager.currentDisplayMenu = [[_baseValues.viewManager.backListMenuAry lastObject]intValue];
    
    /*
     * Remove the current view once animation is finished
     */
    
    [UIView beginAnimations:Nil context:NULL];
    [UIView setAnimationDuration:0.4];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removeCurrentView)];
    
    if(_baseValues.viewManager.currentDisplayMenu==SearchResMenuViewConst)
    {
        _baseValues.searchResultController.frame = CGRectMake(_baseValues.viewManager.screenSize.width, 0, _baseValues.viewManager.screenSize.width, _baseValues.viewManager.screenSize.height);
    }
    
    if(_baseValues.viewManager.currentDisplayMenu == SearchDetMenuViewConst)
    {
        _baseValues.searchResultController.resultDetailController.frame = CGRectMake(_baseValues.viewManager.screenSize.width, 50, _baseValues.viewManager.screenSize.width, _baseValues.viewManager.screenSize.height-50);
    }
    
    [UIView commitAnimations];
    
}

-(void)homeBtnPressedDelegate
{
    
    _baseValues.viewManager.currentDisplayMenu = [[_baseValues.viewManager.backListMenuAry lastObject]intValue];
    /*
     * Remove the current view once animation is finished
     */
    
    [UIView beginAnimations:Nil context:NULL];
    [UIView setAnimationDuration:0.4];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(homeAnimationFinish)];
    
    if(_baseValues.viewManager.currentDisplayMenu==SearchResMenuViewConst)
    {

        _baseValues.searchResultController.frame = CGRectMake(0, _baseValues.viewManager.screenSize.height, _baseValues.viewManager.screenSize.width, _baseValues.viewManager.screenSize.height);
    }
    
    if(_baseValues.viewManager.currentDisplayMenu==SearchDetMenuViewConst)
    {
        //Hides the movie list tableview so it won't show when we do the animation for result detail page
        _baseValues.searchResultController.resultTableController.tableView.hidden = true;
        
         _baseValues.searchResultController.resultDetailController.frame = CGRectMake(0, _baseValues.viewManager.screenSize.height, _baseValues.viewManager.screenSize.width, _baseValues.viewManager.screenSize.height-50);
    }
    
    [UIView commitAnimations];
    
}

@end
