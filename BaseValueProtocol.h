//
//  BaseValueProtocol.h
//  AllDigitalTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//
//  Purpose: Creates all the menu intialize methods

#import <Foundation/Foundation.h>

@protocol BaseValueProtocol <NSObject>

/*
 * Initialize all the menu methods
 */

-(void)searchMenuInitialize;
-(void)searchResultMenuInitialize:(NSString *)searchTerm;

@end
