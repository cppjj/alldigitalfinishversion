//
//  ResultDetailView.h
//  AllDigitalTest
//
//  Created by JJ on 3/29/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//
//  Purpose: Initialize all the search result detail UIs here

#import <UIKit/UIKit.h>
#import "ViewManager.h"
#import "CustomUI.h"
#import "ResultDetailProtocol.h"

@interface ResultDetailView : UIView<ResultDetailProtocol, UIScrollViewDelegate>

@property (nonatomic) id delegate;

//Purchase url
@property (nonatomic, retain) NSString *detailPurchaseURL;

//Preview url
@property (nonatomic, retain) NSString *detailPreviewURL;

/*
 * Classes
 */

@property (nonatomic, retain) ViewManager *viewManager;
@property (nonatomic, retain) CustomUI *customUI;

/*
 * UIScrolView
 */

@property (nonatomic, retain) UIScrollView *detailScrollView;

/*
 * UIImageView
 */

@property (nonatomic, retain) UIImageView *detailArtImgView;

/*
 * UILabel
 */

@property (nonatomic, retain) UILabel *detailTitleLabel;
@property (nonatomic, retain) UILabel *detailReleaseDateLabel;
@property (nonatomic, retain) UILabel *detailPriceLabel;
@property (nonatomic, retain) UILabel *detailRatingLabel;
@property (nonatomic, retain) UILabel *detailLongDesLabel;

/*
 * UIButton
 */

@property (nonatomic, retain) UIButton *detailPreviewBtn;
@property (nonatomic, retain) UIButton *detailPurchaseBtn;

@end
