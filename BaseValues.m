//
//  BaseValues.m
//  AllDigitalTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import "BaseValues.h"

@implementation BaseValues

-(id)init
{
    self = [super init];
    
    if(self)
    {

        _isAnimating = false;
        _conManager = [[ConManager alloc]init];
        _viewManager = [[ViewManager alloc]init];
    }
    
    return self;
}

/*
 * Initialize all the menu methods
 */

-(void)searchMenuInitialize
{
    _searchController = [[SearchController alloc]initWithFrame:CGRectMake(0, 0, _viewManager.screenSize.width, _viewManager.screenSize.height)];
    
    [_searchController searchViewInit:_viewManager];

}

-(void)searchResultMenuInitialize:(NSString *)searchTerm
{
    _searchResultController = [[SearchResultController alloc]initWithFrame:CGRectMake(0, 0, _viewManager.screenSize.width, _viewManager.screenSize.height)];
    
    [_searchResultController searchResultViewInit:_viewManager :_conManager];
    [_searchResultController loadServerMovies:searchTerm];

}

@end
