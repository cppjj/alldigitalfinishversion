//
//  ViewManager.h
//  AllDigitTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//
//  Purpose: Handles all the views controls that is being share by all classes
//

#import <Foundation/Foundation.h>
#import "ViewManagerProtocol.h"
#import "Constant.h"

@interface ViewManager : NSObject<ViewManagerProtocol>
{
    int routeToMenu;
}


//Getter and setter for screen size
@property(nonatomic) CGSize screenSize;

/*
 * Get the current display menu
 */

@property(nonatomic) int currentDisplayMenu;


/*
 * Use this to keep track of the backlist, when user clicks the back button
 */

@property(nonatomic, retain) NSMutableArray *backListMenuAry;

@end
