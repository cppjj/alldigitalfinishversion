//
//  ResultTableViewController.m
//  AllDigitalTest
//
//  Created by JJ on 3/29/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import "ResultTableViewController.h"
#import "SearchResult.h"
#import "Constant.h"
#import "SDWebImage/UIImageView+WebCache.h"

@implementation ResultTableViewController

float lastContentOffset;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
     
        _customUI = [[CustomUI alloc]init];
        
        lastContentOffset = 0.0;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _resultTableArray = [[NSMutableArray alloc]init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    float currentOffset = scrollView.contentOffset.y;
    
    if (currentOffset > lastContentOffset)
    {
        
        [self.delegate scrollDownCallBack];
    }
    else
    {

        [self.delegate scrollUpCallBack];
    }
    
    lastContentOffset = currentOffset;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(_resultTableArray.count > 0)
    {
         return [_resultTableArray count];
    }
    
    else
    {
        return 1;
    }
   
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
   if(_resultTableArray.count > 0)
   {
       @try {
           //Creates a custom view to hold all the cell items
           UIView *customView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 100)];
           
           SearchResult *searchResult = [_resultTableArray objectAtIndex:indexPath.section];
           
           /*
            * Load the image into art image view
            */
           
           UIImageView *artImgView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 80, 80)];
           
           UIActivityIndicatorView *imgLoadIndicator = [_customUI customLoadingIndicator:(artImgView.frame.origin.x + artImgView.frame.size.width)*0.5 :(artImgView.frame.origin.y + artImgView.frame.size.height)*0.5 :60 :60 :[UIColor redColor]];
           
           [artImgView setImageWithURL:[NSURL URLWithString:searchResult.artWorkURLLarge]
                      placeholderImage:nil
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
            {
                [imgLoadIndicator stopAnimating];
                
            }];
           
           /*
            * Loading title and rating
            */
           
           NSString *movieTitleString = @"Title: ";
           movieTitleString = [movieTitleString stringByAppendingString:searchResult.movieTitle];
           
           UILabel *movieTitleLabel = [_customUI customLabel:artImgView.frame.origin.x + artImgView.frame.size.width + 10 :artImgView.frame.origin.y+10 :225 :20 :[UIColor blackColor] :@"Arial" :14.0f];
           movieTitleLabel.text = movieTitleString;
           
           NSString *movieRatingString = @"Rating: ";
           movieRatingString = [movieRatingString stringByAppendingString:searchResult.rating];
           
           UILabel *movieRatingLabel = [_customUI customLabel:artImgView.frame.origin.x + artImgView.frame.size.width + 10 :movieTitleLabel.frame.origin.y + movieTitleLabel.frame.size.height + 20 :210 :20 :[UIColor blackColor] :@"Arial" :14.0f];
           movieRatingLabel.text = movieRatingString;
           
           if(_resultTableArray.count <=4)
           {
                tableView.scrollEnabled = NO;
           }
          
           else
           {
                tableView.scrollEnabled = YES;
           }
           
           [customView addSubview:artImgView];
           [customView addSubview:movieTitleLabel];
           [customView addSubview:movieRatingLabel];
           
           [cell addSubview:customView];
       }
       
       @catch (NSException *exception)
       {
           
       }

   }
    
    else
    {
        cell.textLabel.text = noResultString;
        tableView.scrollEnabled = NO;
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    if(_resultTableArray.count > 0)
       [self.delegate getSelectedResultRow:[_resultTableArray objectAtIndex:indexPath.section]];
}

@end
