//
//  SearchResult.h
//  AllDigitalTest
//
//  Created by JJ on 3/29/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//
//  Purpose: This is the DTO for movie search result

#import <Foundation/Foundation.h>

@interface SearchResult : NSObject

/*
 * Search result DTO
 */

@property(nonatomic, retain) NSString *resultCount;
@property(nonatomic, retain) NSString *movieTitle;
@property(nonatomic, retain) NSString *artWorkURLLarge;
@property(nonatomic, retain) NSString *rating;

/*
 * Result detail DTO
 */

@property(nonatomic, retain) NSString *releaseDate;
@property(nonatomic, retain) NSString *priceCurrency;
@property(nonatomic, retain) NSString *longDescription;
@property(nonatomic, retain) NSString *linkPurchase;
@property(nonatomic, retain) NSString *previewURL;

@end
