//
//  SearchController.h
//  AllDigitTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//
//  Purpose: Handle all the search view events

#import "SearchView.h"

@interface SearchController : SearchView

@end
