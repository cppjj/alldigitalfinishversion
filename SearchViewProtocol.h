//
//  SearchViewProtocol.h
//  AllDigitalTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewManager.h"

@protocol SearchViewProtocol <NSObject>

/*
 * Search Initialize method
 */

-(void)searchViewInit:(ViewManager *)myViewManager;

/*
 * Changes the text for searchBar cancel button
 */

-(void)changeSearchBarCancelBtnTitle;

/*
 * Delegate call back methods when user clicks on the search button
 */

-(void)searchClickDelegate:(NSString *)searchTerm;

@end
