//
//  SearchView.m
//  AllDigitTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import "SearchView.h"

@implementation SearchView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _customUI = [[CustomUI alloc]init];
        
        _histTableViewController = [[SearchHistTableViewController alloc]initWithStyle:UITableViewStyleGrouped];
        _histTableViewController.delegate = self;
    }
    
    return self;
}

/*
 * Search Initialize method
 */

-(void)searchViewInit:(ViewManager *)myViewManager
{
    _viewManager = myViewManager;
    
    _loadHistory = [NSUserDefaults standardUserDefaults];

    NSArray *holdSaveHistory = [_loadHistory objectForKey:@"userhistory"];
    
    /*
     * Gets the recent search history
     */
    
    _histTableViewController.recentHistoryArry = [[NSMutableArray alloc]initWithArray:holdSaveHistory copyItems:YES];
    
    /*
     * Creates the top search bar
     */
    
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, _viewManager.screenSize.width, 50)];
    _searchBar.barTintColor = [UIColor colorWithRed:0.576 green:0.769 blue:0.49 alpha:1.0f];
   // _searchBar.showsCancelButton = NO;
    _searchBar.delegate = self;
    _searchBar.placeholder = @"Search Movie";
    
    [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil]
     setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil]
     forState:UIControlStateNormal];
    
    /*
     * Creates the sub background view when user starts searching
     */
    
    _searchBackgroundSubView = [_customUI customColorBackgroundView:0 :_searchBar.frame.origin.y + _searchBar.frame.size.height :_viewManager.screenSize.width :_viewManager.screenSize.height-_searchBar.frame.size.height :0.0 :[UIColor clearColor] :[UIColor blackColor]];
    _searchBackgroundSubView.alpha = 0.5f;
    _searchBackgroundSubView.hidden = true;
    
    if(holdSaveHistory.count==0 || [[holdSaveHistory objectAtIndex:0]  isEqualToString:noRecentHistory])
    {
           _histTableViewController.tableView.frame = CGRectMake(0, _searchBar.frame.origin.y + _searchBar.frame.size.height, _viewManager.screenSize.width, _viewManager.screenSize.height - _searchBar.frame.size.height );
        _histTableViewController.tableView.userInteractionEnabled = false;
    }
    
    else
    {
        /*
         * Creates the clear history button and history tableview
         */
        _clearHistoryBtn = [_customUI customColorBtn:0 :_viewManager.screenSize.height - 50 :_viewManager.screenSize.width :50 :[UIColor whiteColor] :[UIColor redColor] :[UIColor clearColor] :2.0f];
        [_clearHistoryBtn setTitle:@"Clear History" forState:UIControlStateNormal];
        [_clearHistoryBtn addTarget:self action:@selector(removeHistoryEvent) forControlEvents:UIControlEventTouchUpInside];
        
        _histTableViewController.tableView.frame = CGRectMake(0, _searchBar.frame.origin.y + _searchBar.frame.size.height, _viewManager.screenSize.width, _viewManager.screenSize.height - (_searchBar.frame.size.height + _clearHistoryBtn.frame.size.height));
    }
    
    /*
     * Tap gesture to hide the search background and keyboard
     */
    
    UITapGestureRecognizer *searchTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    
    /*
     * Add to views
     */
    
    [_searchBackgroundSubView addGestureRecognizer:searchTap];
    
    [self addSubview:_searchBar];
    [self addSubview:_histTableViewController.tableView];
    [self addSubview:_clearHistoryBtn];
    [self addSubview:_searchBackgroundSubView];
}

@end
