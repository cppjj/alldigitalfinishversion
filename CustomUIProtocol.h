//
//  CustomUIProtocol.h
//  AllDigitalTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//
//  Purpose: Creates all the customUI methods

#import <Foundation/Foundation.h>

@protocol CustomUIProtocol <NSObject>

/*
 * Custom Views
 */

-(UIView *)customColorBackgroundView :(float)positionX :(float)positionY :(float)viewWidth :(float)viewHeight :(float)cornerRadius :(UIColor *)customBorderColor :(UIColor *)customBackgroundColor;

/*
 * Custom Labels
 */

-(UILabel *)customLabel :(float)positionX :(float)positionY :(float)labelWidth :(float)labelHeight :(UIColor *)textColor :(NSString *)textFont :(float)textSize;

/*
 * Custom Buttons
 */

-(UIButton *)customColorBtn :(float)positionX :(float)positionY :(float) btnWidth :(float)btnHeight :(UIColor *)titleColor :(UIColor *)btnColor : (UIColor *)borderColor :(float)borderWidth;

/*
 * Custom Loading indicator
 */

-(UIActivityIndicatorView *)customLoadingIndicator :(float)positionX :(float)positionY :(float)indicatorWidth :(float)indicatorHeight :(UIColor *)indicatorColor;

@end
