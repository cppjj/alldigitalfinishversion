//
//  SearchView.h
//  AllDigitTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//
//  Purpose: Initialize all the search view UIs here

#import <UIKit/UIKit.h>
#import "CustomUI.h"
#import "SearchViewProtocol.h"
#import "Constant.h"
#import "ViewManager.h"
#import "SearchHistTableViewController.h"

@interface SearchView : UIView<SearchViewProtocol, UISearchBarDelegate>

@property(nonatomic) id delegate;

/*
 * Use to load and save object
 */

@property(nonatomic) NSUserDefaults *loadHistory;

/*
 * Class objects
 */

@property(nonatomic, retain) ViewManager *viewManager;
@property(nonatomic, retain) CustomUI *customUI;

@property(nonatomic, retain) SearchHistTableViewController *histTableViewController;

/*
 * UIView
 */

@property(nonatomic, retain) UIView *searchBackgroundSubView;


/*
 * UISearchbar
 */

@property(nonatomic, retain) UISearchBar *searchBar;

/*
 * UIButton
 */

@property(nonatomic, retain) UIButton *clearHistoryBtn;

@end
