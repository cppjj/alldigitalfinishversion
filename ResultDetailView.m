//
//  ResultDetailView.m
//  AllDigitalTest
//
//  Created by JJ on 3/29/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import "ResultDetailView.h"
#import "SearchResult.h"
#import "Reachability.h"
#import "SDWebImage/UIImageView+WebCache.h"

@implementation ResultDetailView

//Use to keep track if the user is scrolling up or down
float lastContentOffset;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _customUI = [[CustomUI alloc]init];
        
        lastContentOffset = 0.0f;
    }
    return self;
}

/*
 * Search result detail initalize method
 */

-(void)searchResultDetailInit:(ViewManager *)myViewManager :(SearchResult *)holdSearchResult
{
    _viewManager = myViewManager;
    
    /*
     * Initialize the background scroll view
     */
    
    _detailScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, _viewManager.screenSize.width, _viewManager.screenSize.height-50)];
    _detailScrollView.delegate = self;
    _detailScrollView.bounces = false;
    
    _detailArtImgView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 100, 100)];
    
    /*
     * Checking for internet status
     * If there are internet, load image from remote server
     * If there is no internet, load the default local image
     */
    
    Reachability *internetReach = [Reachability reachabilityWithHostname:@"www.google.com"];
    NetworkStatus internetStatus = [internetReach currentReachabilityStatus];

    UIActivityIndicatorView *imgLoadIndicator = [_customUI customLoadingIndicator:(_detailArtImgView.frame.origin.x + _detailArtImgView.frame.size.width)*0.5 :(_detailArtImgView.frame.origin.y + _detailArtImgView.frame.size.height)*0.5 :60 :60 :[UIColor redColor]];
    
    [imgLoadIndicator startAnimating];
    
    //No internet connection
    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN))
    {
         [imgLoadIndicator stopAnimating];
    }
    
    else
    {
        [_detailArtImgView setImageWithURL:[NSURL URLWithString:holdSearchResult.artWorkURLLarge]
                   placeholderImage:nil
                          completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
         {
             [imgLoadIndicator stopAnimating];
             
         }];
    }
    
    /*
     * Initialize all labels
     */
    
    NSString *movieTitleString = @"Title: ";
    movieTitleString = [movieTitleString stringByAppendingString:holdSearchResult.movieTitle];
    _detailTitleLabel = [_customUI customLabel:_detailArtImgView.frame.origin.x + _detailArtImgView.frame.size.width + 5 :_detailArtImgView.frame.origin.y :200 :20 :[UIColor darkGrayColor] :@"Arial" :15.0f];
    _detailTitleLabel.text = movieTitleString;
    
    /*
     * Remove time in release date
     * Conver the date into a different format
     */
    
    //Holds the release date without the time
    NSArray *dateTimeless = [holdSearchResult.releaseDate componentsSeparatedByString:@"T"];
    
    //Create date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *date = [dateFormatter dateFromString:[dateTimeless objectAtIndex:0]];
    
    //Set the date format we want
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"M/d/yyyy"];
    
    //Convert the date into new string
    NSString *newDateString = [dateFormatter stringFromDate:date];
    
    NSString *movieReleaseDateString = @"Release Date: ";
    movieReleaseDateString = [movieReleaseDateString stringByAppendingString:newDateString];
    _detailReleaseDateLabel = [_customUI customLabel:_detailArtImgView.frame.origin.x + _detailArtImgView.frame.size.width + 5 :_detailTitleLabel.frame.origin.y + _detailTitleLabel.frame.size.height :180 :25 :[UIColor darkGrayColor] :@"Arial" :13.0f];
    _detailReleaseDateLabel.text = movieReleaseDateString;
    
    /*
     * Price Label
     */
    NSString *priceString = @"Price: $";
    priceString = [priceString stringByAppendingString:[NSString stringWithFormat:@"%@", holdSearchResult.priceCurrency]];
    priceString = [priceString stringByAppendingString:@" USD"];
    
    _detailPriceLabel = [_customUI customLabel:_detailArtImgView.frame.origin.x + _detailArtImgView.frame.size.width + 5 :_detailReleaseDateLabel.frame.origin.y + _detailReleaseDateLabel.frame.size.height :180 :25 :[UIColor darkGrayColor] :@"Arial" :13.0f];
    _detailPriceLabel.text = priceString;
    
    /*
     * Rating label
     */
    
    NSString *ratingString = @"Rating: ";
    ratingString = [ratingString stringByAppendingString:holdSearchResult.rating];
    
    _detailRatingLabel = [_customUI customLabel:_detailArtImgView.frame.origin.x + _detailArtImgView.frame.size.width + 5 :_detailPriceLabel.frame.origin.y + _detailPriceLabel.frame.size.height :180 :25 :[UIColor darkGrayColor] :@"Arial" :13.0f];
    _detailRatingLabel.text = ratingString;
    
    //Set the purchase url
    _detailPurchaseURL = holdSearchResult.linkPurchase;
    
    //Set the preivew url
    _detailPreviewURL = holdSearchResult.previewURL;
    
    /*
     * Initialize preview ahd purchase UIButtons
     */
    
    if(_detailPreviewURL.length > 0)
    {
        _detailPreviewBtn = [_customUI customColorBtn:_detailArtImgView.frame.origin.x+15 :_detailArtImgView.frame.origin.y + _detailArtImgView.frame.size.height + 15 :120 :40 :[UIColor darkGrayColor] :[UIColor colorWithRed:105.0f/255.0f green:210.f/255.0f blue:232.0f/255.0f alpha:1.0f] :[UIColor darkGrayColor] :1.0f];
        [_detailPreviewBtn setTitle:@"Preview" forState:UIControlStateNormal];
        [_detailPreviewBtn addTarget:self action:@selector(previewBtnReleased) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if(_detailPurchaseURL.length > 0)
    {
        _detailPurchaseBtn = [_customUI customColorBtn:_detailPreviewBtn.frame.origin.x + _detailPreviewBtn.frame.size.width + 30 :_detailArtImgView.frame.origin.y + _detailArtImgView.frame.size.height + 15 :120 :40 :[UIColor whiteColor] :[UIColor colorWithRed:248.0f/255.0f green:105.f/255.0f blue:0.0f/255.0f alpha:1.0f] :[UIColor darkGrayColor] :1.0f];
        [_detailPurchaseBtn setTitle:@"Purchase" forState:UIControlStateNormal];
        [_detailPurchaseBtn addTarget:self action:@selector(purchaseBtnReleased) forControlEvents:UIControlEventTouchUpInside];
   }
    
    /*
     * Lond description label
     */
    

    
    _detailLongDesLabel = [_customUI customLabel:15 :180 :_viewManager.screenSize.width-25 :50 :[UIColor darkGrayColor] :@"Arial" :14.0f];
    _detailLongDesLabel.numberOfLines = 0;
    _detailLongDesLabel.text = holdSearchResult.longDescription;
    [_detailLongDesLabel sizeToFit];

    /*
     * Reset the scroll view content base on the length of the detail label
     */
    
   _detailScrollView.contentSize = CGSizeMake(_viewManager.screenSize.width,_detailLongDesLabel.frame.origin.y + _detailLongDesLabel.frame.size.height);
    
    /*
     * Adding all UIs to the view
     */
    
    [_detailScrollView addSubview:_detailArtImgView];
    [_detailScrollView addSubview:imgLoadIndicator];
    [_detailScrollView addSubview:_detailLongDesLabel];
    [_detailScrollView addSubview:_detailTitleLabel];
    [_detailScrollView addSubview:_detailReleaseDateLabel];
    [_detailScrollView addSubview:_detailPriceLabel];
    [_detailScrollView addSubview:_detailRatingLabel];
    
    [_detailScrollView addSubview:_detailPreviewBtn];
    [_detailScrollView addSubview:_detailPurchaseBtn];
    
    [self addSubview:_detailScrollView];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    float currentOffset = scrollView.contentOffset.y;
    
    if (currentOffset > lastContentOffset)
    {
        
        [self.delegate scrollDownCallBack];
    }
    else
    {
        
        [self.delegate scrollUpCallBack];
    }
    
    lastContentOffset = currentOffset;
}

@end
