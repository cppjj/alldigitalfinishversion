//
//  main.m
//  AllDigital
//
//  Created by JJ on 3/30/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
