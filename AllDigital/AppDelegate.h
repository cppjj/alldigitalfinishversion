//
//  AppDelegate.h
//  AllDigital
//
//  Created by JJ on 3/30/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
