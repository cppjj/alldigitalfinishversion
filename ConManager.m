//
//  ConManager.m
//  AllDigitalTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import "ConManager.h"
#import "AFNetworking.h"

@implementation ConManager

int returnType;

-(id)init
{
    self = [super init];
    
    _responseData = [[NSMutableData alloc] init];
    
    return self;
}


//Handles asychronous loading
-(void)sendAsyRequest:(NSString *)urlString :(int)returnMenu
{
    
    NSURL *jsonUrl = [NSURL URLWithString:urlString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:jsonUrl cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:60.0];
    
    @try {
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             NSDictionary *dictionary = (NSDictionary *)responseObject;
             
             @try {
                 switch (returnMenu)
                 {
                     case SearchResMenuViewConst:
                         [_delegate getSearchResultCallBack:dictionary];
                         break;
                         
                     default:
                         break;
                 }
             }
             
             @catch (NSException *exception)
             {
                 
             }
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             
             UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error retrieving data"
                                                                 message:[error localizedDescription]
                                                                delegate:nil
                                                       cancelButtonTitle:@"Ok"
                                                       otherButtonTitles:nil];
             [alertView show];
         }];
        
        [operation start];
    }
    
    @catch (NSException *exception) {
        
    }
}

@end
