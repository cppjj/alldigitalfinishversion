//
//  SearchResultView.m
//  AllDigitalTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import "SearchResultView.h"

@implementation SearchResultView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _customUI = [[CustomUI alloc]init];
        _jsonDecode = [[JsonDecode alloc]init];
        
        _resultTableController = [[ResultTableViewController alloc]initWithStyle:UITableViewStyleGrouped];
        _resultTableController.delegate = self;
        
        _isAnimating = false;
        _isMovingUp = true;
    }
    return self;
}

/*
 * Initial method for search result view
 */

-(void)searchResultViewInit:(ViewManager *)myViewManager :(ConManager *)myConManger
{
    
    _viewManager = myViewManager;
    _conManager = myConManger;
    _conManager.delegate = self;
    
    /*
     * Title bar
     */
    
    _searchResultTitleBackground = [_customUI customColorBackgroundView:0 :0 :_viewManager.screenSize.width :50 :0.0 :[UIColor clearColor] :[UIColor colorWithRed:0.576 green:0.769 blue:0.49 alpha:1.0f]];
    
    _resultBackArrowLabel = [_customUI customLabel:-17 :0 :80 :50 :[UIColor colorWithRed:95.0f/255.0f green:142.0f/255.0f blue:74.0f/255.0f alpha:1.0f] :@"Helvetica-Bold" :40];
    _resultBackArrowLabel.text = searchResultBackArrow;
    
    _resultBackTextLabel = [_customUI customLabel:25 :0 :60 :50 :[UIColor colorWithRed:95.0f/255.0f green:142.0f/255.0f blue:74.0f/255.0f alpha:1.0f] :@"Helvetica-Bold" :18];
    _resultBackTextLabel.text = searchResultBackString;
    
    _resultTitleLabel = [_customUI customLabel:_resultBackTextLabel.frame.origin.x + _resultBackTextLabel.frame.size.width :0 :160 :50 :[UIColor darkGrayColor] :@"Verdana-Bold" :18.0f];
    _resultTitleLabel.text = searchResultTitle;
    _resultTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    _searchResultHomeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _searchResultHomeBtn.frame = CGRectMake(_viewManager.screenSize.width*0.85, 4, 40, 40);
    [_searchResultHomeBtn setBackgroundImage:[UIImage imageNamed:@"Home"] forState:UIControlStateNormal];
    [_searchResultHomeBtn addTarget:self action:@selector(homeBtnPressed:) forControlEvents:UIControlEventTouchDown];
    [_searchResultHomeBtn addTarget:self action:@selector(homeBtnReleased:) forControlEvents:UIControlEventTouchUpInside];
    
    _searchResultBackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _searchResultBackBtn.frame = CGRectMake(0, 0, 80, 50);
    [_searchResultBackBtn addTarget:self action:@selector(searchResultBackEvent) forControlEvents:UIControlEventTouchDown];
    
    /*
     * Loading indicator
     */
    
    _resultLoadingIndicator = [_customUI customLoadingIndicator:_viewManager.screenSize.width*0.42 :_searchResultTitleBackground.frame.origin.y + _searchResultTitleBackground.frame.size.height + 10 :67 :67 :[UIColor redColor]];
    [_resultLoadingIndicator startAnimating];
    
    /*
     * Initialize tableview that holds the result list
     */
    
    _resultTableController.tableView.frame = CGRectMake(0, _searchResultTitleBackground.frame.origin.y + _searchResultTitleBackground.frame.size.height, _viewManager.screenSize.width, _viewManager.screenSize.height - _searchResultTitleBackground.frame.size.height);
    _resultTableController.tableView.hidden = true;
    _resultTableController.tableView.bounces = false;
    
    [_searchResultTitleBackground addSubview:_resultBackArrowLabel];
    [_searchResultTitleBackground addSubview:_resultBackTextLabel];
    [_searchResultTitleBackground addSubview:_resultTitleLabel];
    [_searchResultTitleBackground addSubview:_searchResultHomeBtn];
    [_searchResultTitleBackground addSubview:_searchResultBackBtn];
    
    [self addSubview:_resultTableController.tableView];
    [self addSubview:_searchResultTitleBackground];
    [self addSubview:_resultLoadingIndicator];
}

/*
 * Use this method to initialize the movie detail view
 */

-(void)detailViewInitMethod :(SearchResult *)holdSearchResult
{
    /*
     * Hides the result table list
     */
    _resultTableController.tableView.hidden = true;
    
    if(_searchResultTitleBackground.frame.origin.y < 0)
    {
        _searchResultTitleBackground.frame = CGRectMake(0, 0, _viewManager.screenSize.width, 50);
    }
    
    _resultDetailController = [[ResultDetailController alloc]initWithFrame:CGRectMake(_viewManager.screenSize.width, 50, self.viewManager.screenSize.width, self.viewManager.screenSize.height - 50)];
    
    _resultDetailController.backgroundColor = [UIColor colorWithRed:239.0f/255.0f green:239.0f/255.0f blue:244.0f/255.0f alpha:1.0f];
    [_resultDetailController searchResultDetailInit:_viewManager :holdSearchResult];
    _resultDetailController.delegate = self;
    
    [_viewManager.backListMenuAry addObject:[_viewManager convertMenu:SearchDetMenuViewConst]];
    
    [self addSubview:_resultDetailController];
    
    /*
     * Handles the slide animation for detail view
     */
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.4];
    
    _resultDetailController.frame = CGRectMake(0, 50, self.viewManager.screenSize.width, self.viewManager.screenSize.height - 50);

    [UIView commitAnimations];
    
}

/*
 * Use this method to remove the movie detail view
 */

-(void)removeDetailViewMethod
{
    /*
     * Display the result table list
     */
    _resultTableController.tableView.hidden = false;

    if(!_isMovingUp)
    {
        _searchResultTitleBackground.frame = CGRectMake(0, -50, _viewManager.screenSize.width, 50);
    }
    
    [_resultDetailController removeFromSuperview];
    _resultDetailController = nil;
    
    [_viewManager.backListMenuAry removeLastObject];
}

@end
