//
//  SearchHistTableViewController.h
//  AllDigitalTest
//
//  Created by JJ on 3/30/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//
//  Purpose: This is where we display the recent search history from the user

#import <UIKit/UIKit.h>
#import "SearchHistTableProtocol.h"

@interface SearchHistTableViewController : UITableViewController<UITableViewDataSource, UITableViewDelegate, SearchHistTableProtocol>

@property(nonatomic) id delegate;

//Holds the recent search history
@property(nonatomic, retain) NSMutableArray *recentHistoryArry;

@end
