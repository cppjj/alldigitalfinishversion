//
//  JsonDecode.h
//  AllDigitalTest
//
//  Created by JJ on 3/29/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//
//  Purpose: Handles decoding all the json return result

#import <Foundation/Foundation.h>
#import "JsonDecodeProtocol.h"

@interface JsonDecode : NSObject<JsonDecodeProtocol>
{
    NSMutableArray *decodeResultArray;
}


@end
