//
//  SearchHistTableViewController.m
//  AllDigitalTest
//
//  Created by JJ on 3/30/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import "SearchHistTableViewController.h"
#import "Constant.h"

@interface SearchHistTableViewController ()

@end

@implementation SearchHistTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(_recentHistoryArry.count > 0)
    {
        return [_recentHistoryArry count];
    }
    
    else
    {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(_recentHistoryArry.count > 0)
    {
        cell.textLabel.text = [_recentHistoryArry objectAtIndex:indexPath.row];
        cell.textLabel.numberOfLines = 2;
        tableView.scrollEnabled = YES;
    }
    
    else
    {
        cell.textLabel.text = noRecentHistory;
        tableView.scrollEnabled = NO;
    }
    
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSUserDefaults *saveArrayDefault = [NSUserDefaults standardUserDefaults];
    
        if(_recentHistoryArry.count==1)
        {
            [_recentHistoryArry replaceObjectAtIndex:0 withObject:noRecentHistory];
            
            //Save the new array
            NSArray *newRecentHistArry = [[NSArray alloc]initWithArray:_recentHistoryArry copyItems:YES];
            [saveArrayDefault setObject:newRecentHistArry forKey:@"userhistory"];
            
            [_delegate removeFinalRowCallback];
            
            [tableView reloadData];
        }
        
        else
        {
            [_recentHistoryArry removeObjectAtIndex:indexPath.row];
            
            //Save the new array
            NSArray *newRecentHistArry = [[NSArray alloc]initWithArray:_recentHistoryArry copyItems:YES];
            [saveArrayDefault setObject:newRecentHistArry forKey:@"userhistory"];
            
            // Delete the row from the data source
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
      
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section==0)
    {
        return @"Search History";
    }
    
    return NULL;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self.delegate recentHistRowSelected:[_recentHistoryArry objectAtIndex:indexPath.row]];
}

@end
