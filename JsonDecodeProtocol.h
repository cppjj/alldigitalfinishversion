//
//  JsonDecodeProtocol.h
//  AllDigitalTest
//
//  Created by JJ on 3/29/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol JsonDecodeProtocol <NSObject>

/*
 * Use this method to decode NSDicionary object for movie result
 */

-(void)decodeMovieInit:(NSDictionary *)decodeDict;

/*
 * Gets the decode result and return it as an array
 */

-(NSMutableArray *)getDecodeResultArry;

@end
