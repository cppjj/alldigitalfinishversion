//
//  ResultDetailProtocol.h
//  AllDigitalTest
//
//  Created by JJ on 3/29/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewManager.h"
#import "SearchResult.h"

@protocol ResultDetailProtocol <NSObject>

/*
 * Search result detail initalize method
 */

-(void)searchResultDetailInit:(ViewManager *)myViewManager :(SearchResult *)holdSearchResult;

/*
 * Scroll back delegate call back
 */

-(void)scrollDownCallBack;
-(void)scrollUpCallBack;

@end
