//
//  ViewManager.m
//  AllDigitTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import "ViewManager.h"

@implementation ViewManager

-(id)init
{
    self = [super init];
    
    if(self)
    {
        //Get the screen size of the device
        _screenSize = [UIScreen mainScreen].bounds.size;
        
        _currentDisplayMenu = SearchMenuViewConst;
        
        //Initialize the backlist menu with to Search menu
        _backListMenuAry = [[NSMutableArray alloc]initWithObjects:[self convertMenu:SearchMenuViewConst], nil];
    
    }
    
    return self;
}

/*
 * The method convert the menu into a string value for storing purpose
 */

-(NSString *)convertMenu:(int)menuSelected
{
    NSString *tempMenu = [NSString stringWithFormat:@"%i", menuSelected];
    
    return tempMenu;
}

@end
