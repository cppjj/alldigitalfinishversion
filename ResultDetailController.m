//
//  ResultDetailController.m
//  AllDigitalTest
//
//  Created by JJ on 3/29/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import "ResultDetailController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "Reachability.h"

@implementation ResultDetailController

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


/*
 * Handle the event when user clicked on the done button
 */

- (void)doneButtonClicked
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [moviePlayer stop];
    [moviePlayer.view removeFromSuperview];
}

/*
 * Handles the event when video is finished playing or if there is an error playing the video
 */

-(void)videoFinishedPlaying:(NSNotification *)notification
{
    NSNumber* reason = [[notification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    UIAlertView *errorAlert;
    
    switch ([reason intValue]) {
        case MPMovieFinishReasonPlaybackEnded:
            [self videoStateChangePlaying];
            break;
        case MPMovieFinishReasonPlaybackError:
            errorAlert = [[UIAlertView alloc]initWithTitle:@"Video Unavailable" message:@"Sorry! the video trailer is currently unavailable" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [errorAlert show];
            
            [[NSNotificationCenter defaultCenter] removeObserver:self];
            
            [moviePlayer stop];
            [moviePlayer.view removeFromSuperview];
            break;
        default:
            break;
    }
        
}

-(void)videoStateChangePlaying
{

    MPMoviePlaybackState playbackState = moviePlayer.playbackState;
    
    if(playbackState == MPMoviePlaybackStateStopped)
    {
        
        UIAlertView *errorAlert;
        
        errorAlert = [[UIAlertView alloc]initWithTitle:@"Video Finished" message:@"Finish Playing" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [errorAlert show];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        
        [moviePlayer stop];
        [moviePlayer.view removeFromSuperview];
    }
}

/*
 * Handles the preview movie event
 */

-(void)previewBtnReleased
{

    @try {
        
        /*
         * Checking for internet status
         * Alert the user if there is no internet connectivity
         */
        
        Reachability *internetReach = [Reachability reachabilityWithHostname:@"www.google.com"];
        NetworkStatus internetStatus = [internetReach currentReachabilityStatus];
        
        if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN))
        {
            UIAlertView *noConnection = [[UIAlertView alloc]initWithTitle:@"No internet connection" message:@"Please connect to the internet to view the movie trailer" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            
            [noConnection show];
            
        }
        
        else
        {
            NSURL *fileURL = [NSURL URLWithString:self.detailPreviewURL];
            
            moviePlayer = [[MPMoviePlayerController alloc]initWithContentURL:fileURL];
            
            [moviePlayer.view setFrame:CGRectMake(0, -100, self.viewManager.screenSize.width, self.viewManager.screenSize.height)];
            
            [self addSubview:moviePlayer.view];
            
            moviePlayer.fullscreen = YES;
            moviePlayer.controlStyle = MPMovieControlStyleNone;
            
            [moviePlayer prepareToPlay];
            
            [moviePlayer play];
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonClicked) name:MPMoviePlayerWillExitFullscreenNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoFinishedPlaying:) name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
        }
        

    }
    
    @catch (NSException *exception) {
        
    }
 
}

/*
 * Handles the purchase event
 */

-(void)purchaseBtnReleased
{
    
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:self.detailPurchaseURL]];
}

@end
