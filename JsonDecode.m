//
//  JsonDecode.m
//  AllDigitalTest
//
//  Created by JJ on 3/29/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import "JsonDecode.h"
#import "SearchResult.h"

@implementation JsonDecode

-(id)init
{
    self = [super init];
    
    if(self)
    {
        
    }
    
    return self;
    
}

/*
 * Use this method to decode NSDicionary object for movie result
 */

-(void)decodeMovieInit:(NSDictionary *)decodeDict
{
    decodeResultArray = [[NSMutableArray alloc]init];
    
    NSString *resultCountString = [decodeDict objectForKey:@"resultCount"];
    int totalResultCount = [resultCountString intValue];
    
    /*
     * Only start parsing if there are results in it
     */
    
    if(totalResultCount > 0)
    {
        NSArray *decodeResultDict = [decodeDict objectForKey:@"results"];
        
        for(NSDictionary *resultDict in decodeResultDict)
        {
            SearchResult *sResult = [[SearchResult alloc]init];
            
            sResult.movieTitle = [resultDict objectForKey:@"trackName"];
            sResult.artWorkURLLarge = [resultDict objectForKey:@"artworkUrl100"];
            sResult.rating = [resultDict objectForKey:@"contentAdvisoryRating"];
            
            sResult.releaseDate = [resultDict objectForKey:@"releaseDate"];
            sResult.priceCurrency = [resultDict objectForKey:@"collectionPrice"];
            sResult.longDescription = [resultDict objectForKey:@"longDescription"];
            sResult.linkPurchase = [resultDict objectForKey:@"collectionViewUrl"];
            sResult.previewURL = [resultDict objectForKey:@"previewUrl"];

            [decodeResultArray addObject:sResult];
        }
    }

}

/*
 * Gets the decode result and return it as an array
 */

-(NSMutableArray *)getDecodeResultArry
{
    return decodeResultArray;
}

@end
