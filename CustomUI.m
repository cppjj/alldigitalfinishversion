//
//  CustomUI.m
//  AllDigitalTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//

#import "CustomUI.h"

@implementation CustomUI

/*
 * Custom Views
 */

-(UIView *)customColorBackgroundView :(float)positionX :(float)positionY :(float)viewWidth :(float)viewHeight :(float)cornerRadius :(UIColor *)customBorderColor :(UIColor *)customBackgroundColor
{
    UIView *customColorView = [[UIView alloc]initWithFrame:CGRectMake(positionX, positionY, viewWidth, viewHeight)];
    customColorView.layer.cornerRadius = cornerRadius;
    customColorView.layer.borderWidth = 1.0f;
    customColorView.layer.borderColor = customBorderColor.CGColor;
    customColorView.layer.shadowOffset = CGSizeMake(6.0f, 2.0f);
    customColorView.layer.shadowOpacity = 1.5f;
    customColorView.layer.shadowColor = [UIColor clearColor].CGColor;
    customColorView.layer.shadowRadius = 2.5f;
    customColorView.layer.anchorPoint = CGPointMake(0.5f, 0.5f);
    [customColorView setBackgroundColor:customBackgroundColor];
    
    return customColorView;
}

/*
 * Custom Labels
 */

-(UILabel *)customLabel :(float)positionX :(float)positionY :(float)labelWidth :(float)labelHeight :(UIColor *)textColor :(NSString *)textFont :(float)textSize
{
    UILabel *customLabel = [[UILabel alloc] initWithFrame:CGRectMake(positionX, positionY, labelWidth, labelHeight)];
    customLabel.textColor = textColor;
    customLabel.backgroundColor = [UIColor clearColor];
    customLabel.font = [UIFont fontWithName:textFont size:textSize];
    
    return customLabel;
}

/*
 * Custom Buttons
 */

-(UIButton *)customColorBtn :(float)positionX :(float)positionY :(float) btnWidth :(float)btnHeight :(UIColor *)titleColor :(UIColor *)btnColor : (UIColor *)borderColor :(float)borderWidth
{
    UIButton *customBorderBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    customBorderBtn.layer.cornerRadius = 2;
    [customBorderBtn setTitleColor:titleColor forState:UIControlStateNormal];
    customBorderBtn.titleLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:18.0f];
    
    customBorderBtn.frame = CGRectMake(positionX,positionY, btnWidth, btnHeight);
    customBorderBtn.clipsToBounds = YES;
    [[customBorderBtn layer] setBorderWidth:borderWidth];
    [[customBorderBtn layer] setBorderColor:borderColor.CGColor];
    
    CGRect rectcustomGray = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rectcustomGray.size);
    CGContextRef contextcustomGray = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(contextcustomGray, [btnColor CGColor]);
    CGContextFillRect(contextcustomGray, rectcustomGray);
    
    UIImage *imagecustomGray = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CGRect rectcustomGray_Highlight = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rectcustomGray_Highlight.size);
    CGContextRef contextcustomGray_Highlight = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(contextcustomGray_Highlight, [btnColor CGColor]);
    CGContextFillRect(contextcustomGray_Highlight, rectcustomGray_Highlight);
    
    UIImage *imagecustomGray_Highlight = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [customBorderBtn setBackgroundImage:imagecustomGray_Highlight forState:UIControlStateSelected];
    [customBorderBtn setBackgroundImage:imagecustomGray forState:UIControlStateNormal];
    
    return customBorderBtn;
}

/*
 * Custom Loading indicator
 */

-(UIActivityIndicatorView *)customLoadingIndicator :(float)positionX :(float)positionY :(float)indicatorWidth :(float)indicatorHeight :(UIColor *)indicatorColor
{
    UIActivityIndicatorView *loadingIndicatorView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    loadingIndicatorView.frame = CGRectMake(positionX, positionY, indicatorWidth, indicatorHeight);
    loadingIndicatorView.color = indicatorColor;
    
    return loadingIndicatorView;
}

@end
