//
//  BaseController.h
//  AllDigitTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//
//  Purpose: The base controller handles all the event that are being share by all the views
//           All the views are added in this controller

#import <UIKit/UIKit.h>
#import "BaseValues.h"
#import "Constant.h"
#import "ViewManager.h"
#import "SearchController.h"

@interface BaseController : UIViewController


/*
 * Class objects
 */
@property(nonatomic, retain) BaseValues *baseValues;

@end
