//
//  CustomUI.h
//  AllDigitalTest
//
//  Created by JJ on 3/28/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//
// Purpose: Creates all the custom UI such as views, buttons, and labels

#import <Foundation/Foundation.h>
#import "CustomUIProtocol.h"

@interface CustomUI : NSObject<CustomUIProtocol>


@end
