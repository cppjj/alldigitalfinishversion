//
//  ResultTableViewController.h
//  AllDigitalTest
//
//  Created by JJ on 3/29/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//
//  Purpose: Display the result from movie search

#import <UIKit/UIKit.h>
#import "CustomUI.h"
#import "ResultTableProtocol.h"

@interface ResultTableViewController : UITableViewController<UITableViewDataSource, UITableViewDelegate, ResultTableProtocol, UIScrollViewDelegate>

@property(nonatomic) id delegate;

/*
 * Classes
 */

@property(nonatomic, retain) CustomUI *customUI;

//Holds the search result values to display in table view
@property(nonatomic, retain) NSMutableArray *resultTableArray;

@end
