//
//  ResultDetailController.h
//  AllDigitalTest
//
//  Created by JJ on 3/29/14.
//  Copyright (c) 2014 Jay Lei. All rights reserved.
//
//  Purpose: Handle all the events for result detail view

#import "ResultDetailView.h"
#import <MediaPlayer/MediaPlayer.h>

@interface ResultDetailController : ResultDetailView
{
    MPMoviePlayerController *moviePlayer;
}


@end
